<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Helpers\Helper;

use App\User;

use App\Provider;

use App\Document;

use App\Currency;

use App\ProviderDocument;

use App\ProviderRating;

use App\PromoCode;

use App\HourlyPackage;

use App\ChatMessage;

use App\Admin;

use App\ServiceType;

use App\AirportDetail;

use App\LocationDetail;

use App\AirportPrice;

use App\Requests;

use App\UserRating;

use App\RequestPayment;

use App\ProviderService;

use App\Settings;

use Validator;

use Hash;

use Mail;

use DB;

use Auth;

use Redirect;

use Setting;

use Twilio\Rest\Client;

use App\Corporate;

define('UNIT_DISTANCE', 'kms,miles');
if (!defined('USER')) define('USER',1);
if (!defined('PROVIDER')) define('PROVIDER',1);

if (!defined('NONE')) define('NONE', 0);

if (!defined('DEFAULT_FALSE')) define('DEFAULT_FALSE', 0);
if (!defined('DEFAULT_TRUE')) define('DEFAULT_TRUE', 1);

// Payment Constants
if (!defined('COD')) define('COD',   'cod');
if (!defined('PAYPAL')) define('PAYPAL', 'paypal');
if (!defined('CARD')) define('CARD',  'card');

if (!defined('REQUEST_NEW')) define('REQUEST_NEW',        0);
if (!defined('REQUEST_WAITING')) define('REQUEST_WAITING',      1);
if (!defined('REQUEST_INPROGRESS')) define('REQUEST_INPROGRESS',    2);
if (!defined('REQUEST_COMPLETE_PENDING')) define('REQUEST_COMPLETE_PENDING',  3);
if (!defined('REQUEST_RATING')) define('REQUEST_RATING',      4);
if (!defined('REQUEST_COMPLETED')) define('REQUEST_COMPLETED',      5);
if (!defined('REQUEST_CANCELLED')) define('REQUEST_CANCELLED',      6);
if (!defined('REQUEST_NO_PROVIDER_AVAILABLE')) define('REQUEST_NO_PROVIDER_AVAILABLE',7);
if (!defined('WAITING_FOR_PROVIDER_CONFRIMATION_COD')) define('WAITING_FOR_PROVIDER_CONFRIMATION_COD',  8);


// Only when manual request
if (!defined('REQUEST_REJECTED_BY_PROVIDER')) define('REQUEST_REJECTED_BY_PROVIDER', 9);

if (!defined('PROVIDER_NOT_AVAILABLE')) define('PROVIDER_NOT_AVAILABLE', 0);
if (!defined('PROVIDER_AVAILABLE')) define('PROVIDER_AVAILABLE', 1);

if (!defined('PROVIDER_NONE')) define('PROVIDER_NONE', 0);
if (!defined('PROVIDER_ACCEPTED')) define('PROVIDER_ACCEPTED', 1);
if (!defined('PROVIDER_STARTED')) define('PROVIDER_STARTED', 2);
if (!defined('PROVIDER_ARRIVED')) define('PROVIDER_ARRIVED', 3);
if (!defined('PROVIDER_SERVICE_STARTED')) define('PROVIDER_SERVICE_STARTED', 4);
if (!defined('PROVIDER_SERVICE_COMPLETED')) define('PROVIDER_SERVICE_COMPLETED', 5);
if (!defined('PROVIDER_RATED')) define('PROVIDER_RATED', 6);

if (!defined('REQUEST_META_NONE')) define('REQUEST_META_NONE',   0);
if (!defined('REQUEST_META_OFFERED')) define('REQUEST_META_OFFERED',   1);
if (!defined('REQUEST_META_TIMEDOUT')) define('REQUEST_META_TIMEDOUT', 2);
if (!defined('REQUEST_META_DECLINED')) define('REQUEST_META_DECLINED', 3);

if (!defined('RATINGS')) define('RATINGS', '0,1,2,3,4,5');

if (!defined('DEVICE_ANDROID')) define('DEVICE_ANDROID', 'android');
if (!defined('DEVICE_IOS')) define('DEVICE_IOS', 'ios');

if (!defined('WAITING_TO_RESPOND')) define('WAITING_TO_RESPOND', 1);
if (!defined('WAITING_TO_RESPOND_NORMAL')) define('WAITING_TO_RESPOND_NORMAL',0);

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin', array('except' => ['send_app_link']));
    }

    /**
     * Show the admin dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard() {

        $total = RequestPayment::sum('total');

        $paypal_total = RequestPayment::where('payment_mode','paypal')->sum('total');

        $card_total = RequestPayment::where('payment_mode','card')->sum('total');

        $cod_total = RequestPayment::where('payment_mode','cod')->sum('total');


        $total_requests = Requests::count();

        $completed = Requests::where('status','5')->count();

        $ongoing = Requests::where('status','4')->count();

        $cancelled = Requests::where('status','6')->count();


        $provider_reviews = UserRating::leftJoin('providers', 'user_ratings.provider_id', '=', 'providers.id')
                            ->leftJoin('users', 'user_ratings.user_id', '=', 'users.id')
                            ->select('user_ratings.id as review_id', 'user_ratings.rating', 'user_ratings.comment', 'users.first_name as user_first_name', 'users.last_name as user_last_name', 'providers.first_name as provider_first_name', 'providers.last_name as provider_last_name', 'users.id as user_id', 'users.picture as user_picture', 'providers.id as provider_id', 'user_ratings.created_at')
                            ->orderBy('user_ratings.created_at', 'desc')
                            ->get();

        $get_registers = get_register_count();
        $recent_users = get_recent_users();
        $total_revenue = 10;

        $view = last_days(10);

        return view('admin.dashboard')
                ->with('total' , $total)
                ->with('paypal_total' , $paypal_total)
                ->with('card_total' , $card_total)
                ->with('cod_total' , $cod_total)
                ->with('view' , $view)
                ->with('get_registers' , $get_registers)
                ->with('recent_users' , $recent_users)
                ->with('provider_reviews' , $provider_reviews)
                ->with('total_requests' , $total_requests)
                ->with('completed' , $completed)
                ->with('cancelled' , $cancelled)
                ->with('ongoing' , $ongoing)
                ->withPage('dashboard')
                ->with('sub_page','');
    }

    public function profile() {

        $admin = Admin::first();
        return view('admin.profile')->with('admin' , $admin)->withPage('profile')->with('sub_page','');
    }

    public function profile_process(Request $request) {

        $validator = Validator::make( $request->all(),array(
                'name' => 'max:255',
                'email' => 'email|max:255',
                'mobile' => 'digits_between:6,13',
                'address' => 'max:300',
                'id' => 'required|exists:admins,id'
            )
        );

        if($validator->fails()) {
            $error_messages = implode(',', $validator->messages()->all());
            return back()->with('flash_errors', $error_messages);
        } else {

            $admin = Admin::find($request->id);

            $admin->name = $request->has('name') ? $request->name : $admin->name;

            $admin->email = $request->has('email') ? $request->email : $admin->email;

            $admin->mobile = $request->has('mobile') ? $request->mobile : $admin->mobile;

            $admin->gender = $request->has('gender') ? $request->gender : $admin->gender;

            $admin->address = $request->has('address') ? $request->address : $admin->address;

            if($request->hasFile('picture')) {
                Helper::delete_picture($admin->picture);
                $admin->picture = Helper::normal_upload_picture($request->picture);
            }

            $admin->remember_token = Helper::generate_token();
            $admin->is_activated = 1;
            $admin->save();

            return back()->with('flash_success', Helper::tr('admin_not_profile'));

        }

    }

    public function send_app_link(Request $request)
    {
        $validator = Validator::make($request->all(), [
                'mobile' => 'required|numeric',
            ]);
        if($validator->fails()) {
            $error_messages = implode(',',$validator->messages()->all());
            return back()->with('flash_errors', $error_messages);
        } 
        else 
        {
            $getphone = str_replace('+', '', $request->mobile);
            if (substr($getphone, 0, 1) === '1'){
                $getphone = ltrim($getphone, '1');
            }
            $phone      = "+1".$getphone; 
            // echo $phone; exit;
            $six_digit_random_number = mt_rand(100000, 999999);
            $message    = "Thanks for contacting us. Please install the User App by using this link. ".env('GOOGLE_STORE_USER'); 
            Helper::send_twilio_sms($phone, $message);
            return back()->with('flash_success', 'App links has sent to your phone number.');
        }
    }

    public function change_password(Request $request) {

        $old_password = $request->old_password;
        $new_password = $request->password;
        $confirm_password = $request->confirm_password;

        $validator = Validator::make($request->all(), [
                'password' => 'required|min:6',
                'old_password' => 'required',
                'confirm_password' => 'required|min:6',
                'id' => 'required|exists:admins,id'
            ]);

        if($validator->fails()) {

            $error_messages = implode(',',$validator->messages()->all());

            return back()->with('flash_errors', $error_messages);

        } else {

            $admin = Admin::find($request->id);

            if(Hash::check($old_password,$admin->password))
            {
                $admin->password = Hash::make($new_password);
                $admin->save();

                return back()->with('flash_success', "Password Changed successfully");

            } else {
                return back()->with('flash_error', "Pasword is mismatched");
            }
        }

        $response = response()->json($response_array,$response_code);

        return $response;
    }

    public function payment()
    {
        $payment = DB::table('request_payments')
                    ->leftJoin('requests','requests.id','=','request_payments.request_id')
                    ->leftJoin('users','users.id','=','requests.user_id')
                    ->leftJoin('providers','providers.id','=','requests.confirmed_provider')
                    ->select('request_payments.*','users.first_name as user_first_name','users.last_name as user_last_name','providers.first_name as provider_first_name','providers.last_name as provider_last_name')
                    ->orderBy('created_at','desc')
                    ->get();

        return view('admin.payments')->with('payments',$payment)
                    ->withPage('payments')->with('sub_page','');
    }

    public function paymentSettings()
    {
        $settings = Settings::all();
        return view('admin.paymentSettings')->with('setting',$settings);
    }

    // Corporate Functions

    public function corporates()
    {
        $corporates = Corporate::orderBy('created_at' , 'desc')->get();

        return view('admin.corporates')->with('corporates',$corporates)
                                      ->withPage('corporates')->with('sub_page','view-corporate');;
    }

    public function add_corporate()
    {
        $service_type = ServiceType::all();
        return view('admin.add-corporate')->with('service_types',$service_type)->withPage('corporates')->with('sub_page','add-corporate');
    }

    public function add_corporate_process(Request $request)
    {
        $name = $request->name;
        $email = $request->email;
        $mobile = $request->mobile;
        $gender = $request->gender;
        $picture = $request->file('picture');
        $address = $request->address;

        if($request->id != '')
        {
            $validator = Validator::make(
                $request->all(),
                array(
                    'name' => 'required|max:255',
                    'email' => 'required|email|max:255',
                    'mobile' => 'required|digits_between:6,13',
                    'address' => 'required|max:300',

                )
            );
        }
        else
        {
            $validator = Validator::make(
                $request->all(),
                array(
                    'name' => 'required|max:255',
                    'email' => 'required|email|max:255|unique:corporates,email',
                    'mobile' => 'required|digits_between:6,13',
                    'address' => 'required|max:300',
                    'picture' => 'required|mimes:jpeg,jpg,bmp,png',

                )
            );
        }

        if($validator->fails())
        {
            $error_messages = implode(',', $validator->messages()->all());
            return back()->with('flash_errors', $error_messages);
        }
        else
        {
              if($request->id != '')
              {
                  // Edit corporate
                  $corporate = Corporate::find($request->id);
                  if($picture != ''){
                  $corporate->picture = Helper::upload_picture($picture);
                  }
              }
              else
              {
                  //Add New corporate
                  $corporate = new Corporate;
                  $new_password = time();
                  $new_password .= rand();
                  $new_password = sha1($new_password);
                  $new_password = substr($new_password, 0, 8);
                  $corporate->password = Hash::make($new_password);
                  $corporate->picture = Helper::upload_picture($picture);
              }
              $corporate->name = $name;
              $corporate->email = $email;
              $corporate->mobile = $mobile;
              $corporate->gender = $gender;
              $corporate->is_activated = 1;
              $corporate->is_approved = 1;
              $corporate->paypal_email = $request->paypal_email;
              $corporate->address = $address;


            if($request->id == ''){

            $subject = Helper::tr('corporate_welcome_title');
            $page = "emails.admin.welcome";
            $email_data['first_name'] = $corporate->name;
                $email_data['last_name'] = "";
                $email_data['password'] = $new_password;
                $email_data['email'] = $corporate->email;
            $email = $corporate->email;
            Helper::send_email($page,$subject,$email,$email_data);
            }

            $corporate->save();

            if($corporate)
            {
                return back()->with('flash_success', tr('admin_not_corporate'));
            }
            else
            {
                return back()->with('flash_error', tr('admin_not_error'));
            }
        }
    }

    public function edit_corporate(Request $request)
    {
        $corporate = Corporate::find($request->id);
       
        $service_types = ServiceType::all();
        return view('admin.add-corporate')->with('name', 'Edit corporate')->with('corporate',$corporate)->with('service_types',$service_types)
                                  ->withPage('corporates')->with('sub_page','view-corporate');
    }

    public function corporate_approve(Request $request)
    {
        $corporates = Corporate::orderBy('created_at' , 'asc')->get();
        $corporate = Corporate::find($request->id);
        $corporate->is_approved = $request->status;
        $corporate->save();
        if($request->status ==1)
        {
            $message = tr('admin_not_corporate_approve');
        }
        else
        {
            $message = tr('admin_not_corporate_decline');
        }
        return back()->with('flash_success', $message)->with('corporates',$corporates);
    }

    public function delete_corporate(Request $request)
    {

        if($corporate = Corporate::find($request->id))
        {

            $corporate = Corporate::find($request->id)->delete();
        }

        if($corporate)
        {
            return back()->with('flash_success',tr('admin_not_corporate_del'));
        }
        else
        {
            return back()->with('flash_error',tr('admin_not_error'));
        }
    }

    public function corporate_details(Request $request)
    {
        $corporate = Corporate::find($request->id);
      
        if($corporate) {

            return view('admin.corporate-details')->with('corporate' , $corporate)
                      ->withPage('corporates')->with('sub_page','view-corporate');
        } else {
            return back()->with('error' , "corporate details not found");
        }
    }

    //User Functions

    public function users()
    {
        $user = User::orderBy('created_at' , 'desc')->get();

        return view('admin.users')->withPage('users')->with('sub_page','view-user')->with('users',$user);
    }

    public function add_user()
    {
        return view('admin.add-user')->withPage('users')->with('sub_page','add-user');
    }

    public function add_user_process(Request $request)
    {
            $first_name = $request->first_name;
            $last_name = $request->last_name;
            $email = $request->email;
            $mobile = $request->mobile;
            $gender = $request->gender;
            $picture = $request->file('picture');
            $address = $request->address;

            if($request->id != '')
            {
                $validator = Validator::make(
                    $request->all(),
                    array(
                        'first_name' => 'required|max:255',
                        'last_name' => 'required|max:255',
                        'email' => 'required|email|max:255',
                        'mobile' => 'required|digits_between:6,13',
                        'address' => 'required|max:300',

                    )
                );
            }
            else
            {
                $validator = Validator::make(
                    $request->all(),
                    array(
                        'first_name' => 'required|max:255',
                        'last_name' => 'required|max:255',
                        'email' => 'required|email|max:255|unique:users,email',
                        'mobile' => 'required|digits_between:6,13',
                        'address' => 'required|max:300',
                        'picture' => 'required|mimes:jpeg,jpg,bmp,png',

                    )
                );
            }

        if($validator->fails())
        {
            $error_messages = implode(',', $validator->messages()->all());
            return back()->with('flash_errors', $error_messages);
        }
        else
        {

                if($request->id != '')
                {
                    // Edit User
                    $user = User::find($request->id);
                    if($picture != ''){
                    $user->picture = Helper::upload_picture($picture);
                    }
                }
                else
                {
                    //Add New User
                    $user = new User;
                    $new_password = time();
                    $new_password .= rand();
                    $new_password = sha1($new_password);
                    $new_password = substr($new_password, 0, 8);
                    $user->password = Hash::make($new_password);
                    $user->picture = Helper::upload_picture($picture);
                }
                    $user->first_name = $first_name;
                    $user->last_name = $last_name;
                    $user->email = $email;
                    $user->mobile = $mobile;
                    $user->token = Helper::generate_token();
                    $user->token_expiry = Helper::generate_token_expiry();
                    $user->gender = $gender;
                    $user->is_activated = 1;
                    $user->is_approved = 1;
                    $user->payment_mode = 1;
                    $user->address = $address;


                    if($request->id == ''){
                    $email_data['first_name'] = $user->first_name;
                    $email_data['last_name'] = $user->last_name;
                    $email_data['password'] = $new_password;
                    $email_data['email'] = $user->email;

                    $subject = Helper::tr('user_welcome_title');
                    $page = "emails.admin.welcome";
                    $email = $user->email;
                    Helper::send_email($page,$subject,$email,$email_data);
                    }

                    $user->save();

                if($user)
                {
                    return back()->with('flash_success', tr('admin_not_user'));
                }
                else
                {
                    return back()->with('flash_error', tr('admin_not_error'));
                }

            }
    }

    public function edit_user(Request $request)
    {
        $user = User::find($request->id);
        return view('admin.add-user')->with('name', 'Edit User')->with('user',$user)
                      ->withPage('users')->with('sub_page','view-user');
    }

    public function delete_user(Request $request)
    {

        if($user = User::find($request->id))
        {

            $user = User::find($request->id)->delete();
        }

        if($user)
        {
            return back()->with('flash_success',tr('admin_not_user_del'));
        }
        else
        {
            return back()->with('flash_error',tr('admin_not_error'));
        }
    }


    //Provider Functions

    public function providers()
    {
        $subQuery = DB::table('requests')
                ->select(DB::raw('count(*)'))
                ->whereRaw('confirmed_provider = providers.id and status != 0');
        $subQuery1 = DB::table('requests')
                ->select(DB::raw('count(*)'))
                ->whereRaw('confirmed_provider = providers.id and status in (1,2,3,4,5)');
        $providers = DB::table('providers')
                ->select('providers.*', DB::raw("(" . $subQuery->toSql() . ") as 'total_requests'"), DB::raw("(" . $subQuery1->toSql() . ") as 'accepted_requests'"))
                ->orderBy('providers.id', 'DESC')
                ->get();

        return view('admin.providers')->with('providers',$providers)
                                      ->withPage('providers')->with('sub_page','view-provider');;
    }

    public function add_provider()
    {
        $service_type = ServiceType::all();
        $corporates = Corporate::all();
        return view('admin.add-provider')->with('service_types',$service_type)->withPage('providers')->with('sub_page','add-provider')
                            ->with('corporates',$corporates);
    }

    public function add_provider_process1(Request $request)
    {
        $first_name = $request->first_name;
        $last_name = $request->last_name;
        $email = $request->email;
        $mobile = $request->mobile;
        $gender = $request->gender;
        $picture = $request->file('picture');
        $address = $request->address;

        if($request->id != '')
        {
            $validator = Validator::make(
                $request->all(),
                array(
                    'first_name' => 'required|max:255',
                    'last_name' => 'required|max:255',
                    'email' => 'required|email|max:255',
                    'mobile' => 'required|digits_between:6,13',
                    'address' => 'required|max:300',

                )
            );
        }
        else
        {
            $validator = Validator::make(
                $request->all(),
                array(
                    'first_name' => 'required|max:255',
                    'last_name' => 'required|max:255',
                    'email' => 'required|email|max:255|unique:providers,email',
                    'mobile' => 'required|digits_between:6,13',
                    'address' => 'required|max:300',
                    'picture' => 'mimes:jpeg,jpg,bmp,png',

                )
            );
        }

        if($validator->fails())
        {
            $error_messages = implode(',', $validator->messages()->all());
            return back()->with('flash_errors', $error_messages);
        }
        else
        {
              if($request->id != '')
              {
                  // Edit Provider
                  $provider = Provider::find($request->id);
                  if($picture != ''){
                  $provider->picture = Helper::upload_picture($picture);
                  }
              }
              else
              {
                  //Add New Provider
                  $provider = new Provider;
                  $new_password = time();
                  $new_password .= rand();
                  $new_password = sha1($new_password);
                  $new_password = substr($new_password, 0, 8);
                  $provider->password = Hash::make($new_password);
                  $provider->picture = Helper::upload_picture($picture);
              }
              $provider->first_name = $first_name;
              $provider->last_name = $last_name;
              $provider->email = $email;
              $provider->mobile = $mobile;
              $provider->token = Helper::generate_token();
              $provider->token_expiry = Helper::generate_token_expiry();
              $provider->gender = $gender;
              $provider->is_activated = 1;
              $provider->is_approved = 1;
              $provider->paypal_email = $request->paypal_email;
              $provider->address = $address;
              $provider->corporate_id = $request->corporate;


              if($request->id == ''){

                $subject = Helper::tr('provider_welcome_title');
                $page = "emails.admin.welcome";
                $email_data['first_name'] = $provider->first_name;
                    $email_data['last_name'] = $provider->last_name;
                    $email_data['password'] = $new_password;
                    $email_data['email'] = $provider->email;
                $email = $provider->email;
                Helper::send_email($page,$subject,$email,$email_data);
                }

               $provider->save();

               if($provider) {

                    if($request->has('service_type')) {

                        $check_provider_service = ProviderService::where('provider_id' , $provider->id)
                                                ->first();

                        if(!$check_provider_service) {
                            $provider_service = new ProviderService;
                        } else {
                            $provider_service = $check_provider_service;
                        }

                        $provider_service->provider_id = $provider->id;
                        $provider_service->service_type_id = $request->service_type;
                        $provider_service->is_available = DEFAULT_TRUE;
                        $provider_service->save();
                    }
                }

                if($provider)
                {
                    return back()->with('flash_success', tr('admin_not_provider'));
                }
                else
                {
                    return back()->with('flash_error', tr('admin_not_error'));
                }
            }
    }
    public function add_provider_process(Request $request)
    {
        $first_name = $request->first_name;
        $last_name = $request->last_name;
        $email = $request->email;
        $mobile = $request->mobile;
        $gender = $request->gender;
        $picture = $request->file('picture');
        $address = $request->address;
        $vehicle_no = $request->vehicle_no;
        if($request->id != '')
        {
            $validator = Validator::make(
                $request->all(),
                array(
                    'first_name' => 'required|max:255',
                    'last_name' => 'required|max:255',
                    'email' => 'required|email|max:255',
                    'mobile' => 'required|digits_between:6,13',
                    'address' => 'required|max:300',
                    'vehicle_no' => 'required'
                )
            );
        }
        else
        {
            $validator = Validator::make(
                $request->all(),
                array(
                    'first_name' => 'required|max:255',
                    'last_name' => 'required|max:255',
                    'email' => 'required|email|max:255|unique:providers,email',
                    'mobile' => 'required|digits_between:6,13',
                    'address' => 'required|max:300',
                    'picture' => 'mimes:jpeg,jpg,bmp,png',
                    'vehicle_no' => 'required'
                )
            );
        }

        if($validator->fails())
        {
            $error_messages = implode(',', $validator->messages()->all());
            return back()->with('flash_errors', $error_messages);
        }
        else
        {
              if($request->id != '')
              {
                  // Edit Provider
                  $provider = Provider::find($request->id);
                  if($picture != ''){
                  $provider->picture = Helper::upload_picture($picture);
                  }
              }
              else
              {
                  //Add New Provider
                  $provider = new Provider;
                  $new_password = time();
                  $new_password .= rand();
                  $new_password = sha1($new_password);
                  $new_password = substr($new_password, 0, 8);
                  $provider->password = Hash::make($new_password);
                  $provider->picture = Helper::upload_picture($picture);
              }
              $provider->first_name = $first_name;
              $provider->last_name = $last_name;
              $provider->email = $email;
              $provider->mobile = $mobile;
              $provider->vehicle_no = $vehicle_no;
              $provider->token = Helper::generate_token();
              $provider->token_expiry = Helper::generate_token_expiry();
              $provider->gender = $gender;
              $provider->is_activated = 1;
              $provider->is_approved = 1;
              $provider->paypal_email = $request->paypal_email;
              $provider->address = $address;
              $provider->corporate_id = $request->corporate;


              if($request->id == ''){

                $subject = Helper::tr('provider_welcome_title');
                $page = "emails.admin.welcome";
                $email_data['first_name'] = $provider->first_name;
                    $email_data['last_name'] = $provider->last_name;
                    $email_data['password'] = $new_password;
                    $email_data['email'] = $provider->email;
                $email = $provider->email;
                Helper::send_email($page,$subject,$email,$email_data);
                }

               $provider->save();

               if($provider) {

                    if($request->has('service_type')) {

                        $check_provider_service = ProviderService::where('provider_id' , $provider->id)
                                                ->first();

                        if(!$check_provider_service) {
                            $provider_service = new ProviderService;
                        } else {
                            $provider_service = $check_provider_service;
                        }

                        $provider_service->provider_id = $provider->id;
                        $provider_service->service_type_id = $request->service_type;
                        $provider_service->is_available = DEFAULT_TRUE;
                        $provider_service->save();
                    }
                }

                if($provider)
                {
                    return back()->with('flash_success', tr('admin_not_provider'));
                }
                else
                {
                    return back()->with('flash_error', tr('admin_not_error'));
                }
            }
    }

    public function edit_provider(Request $request)
    {
        $provider = Provider::find($request->id);
        $corporates = Corporate::all();
        $check_provider_service = ProviderService::where('provider_id' , $request->id)->first();
        if($check_provider_service)
            $provider_type = $check_provider_service->service_type_id;
        else
            $provider_type = "";
        $service_types = ServiceType::all();
        return view('admin.add-provider')->with('name', 'Edit Provider')->with('provider_type',$provider_type)->with('provider',$provider)->with('service_types',$service_types)
                                  ->withPage('providers')->with('sub_page','view-provider')
                                  ->with('corporates',$corporates);
    }

    public function provider_documents(Request $request) {
        $provider_id = $request->id;
        $provider = Provider::find($provider_id);
        $documents = Document::all();
        $provider_document = DB::table('provider_documents')
                            ->leftJoin('documents', 'provider_documents.document_id', '=', 'documents.id')
                            ->select('provider_documents.*', 'documents.name as document_name')
                            ->where('provider_id', $provider_id)->get();


        return view('admin.provider-document')
                        ->with('provider', $provider)
                        ->with('document', $documents)
                        ->with('documents', $provider_document)
                        ->withPage('providers')->with('sub_page','view-provider');
    }

    public function Provider_approve(Request $request)
    {
        $providers = Provider::orderBy('created_at' , 'asc')->get();
        $provider = Provider::find($request->id);
        $provider->is_approved = $request->status;
        $provider->save();
        if($request->status ==1)
        {
            $message = tr('admin_not_provider_approve');
        }
        else
        {
            $message = tr('admin_not_provider_decline');
        }
        return back()->with('flash_success', $message)->with('providers',$providers);
    }

    public function delete_provider(Request $request)
    {

        if($provider = Provider::find($request->id))
        {

            $provider = Provider::find($request->id)->delete();
        }

        if($provider)
        {
            return back()->with('flash_success',tr('admin_not_provider_del'));
        }
        else
        {
            return back()->with('flash_error',tr('admin_not_error'));
        }
    }

    public function settings()
    {
        $settings = Settings::all();
        switch (Setting::get('currency')) {
            case '$':
                $symbol = '$';
                $currency = 'US Dollar (USD)';
                break;

	    case 'R':
                $symbol = 'R';
                $currency = 'South African Rand';
                break;
	
            case '₹':
                $symbol = '₹';
                $currency = 'Indian Rupee (INR)';
                break;
            case 'د.ك':
                $symbol = 'د.ك';
                $currency = 'Kuwaiti Dinar (KWD)';
                break;
            case 'د.ب':
                $symbol = 'د.ب';
                $currency = 'Bahraini Dinar (BHD)';
                break;
            case '﷼':
                $symbol = '﷼';
                $currency = 'Omani Rial (OMR)';
                break;
            case '£':
                $symbol = '£';
                $currency = 'Euro (EUR)';
                break;
            case '€':
                $symbol = '€';
                $currency = 'British Pound (GBP)';
                break;
            case 'ل.د':
                $symbol = 'ل.د';
                $currency = 'Libyan Dinar (LYD)';
                break;
            case 'B$':
                $symbol = 'B$';
                $currency = 'Bruneian Dollar (BND)';
                break;
            case 'S$':
                $symbol = 'S$';
                $currency = 'Singapore Dollar (SGD)';
                break;
            case 'AU$':
                $symbol = 'AU$';
                $currency = 'Australian Dollar (AUD)';
                break;
            case 'CHF':
                $symbol = 'CHF';
                $currency = 'Swiss Franc (CHF)';
                break;
            default:
                $symbol = '$';
                $currency = 'US Dollar (USD)';
                break;
        }
        return view('admin.settings')->with('symbol',$symbol)->with('currency',$currency)
                            ->withPage('settings')->with('sub_page','');
    }


    public function settings_process(Request $request)
    {

        


        /*
            PayGate settings save 
        */

        if(($request->has('paygate_id') && $request->paygate_id != '')  
            && ($request->has('paygate_encryption_key') && $request->paygate_encryption_key != '') ) {

            //inserting payagte payment mode
            if(! ($payGate = Settings::where('key', 'paygate')->first()) ) {
                $payGate = new Settings;
                $payGate->key = 'paygate';
            } 
            $payGate->value = 1;
            $payGate->save();


            if(! ($pgId = Settings::where('key', 'paygate_id')->first()) ) {
                $pgId = new Settings;
                $pgId->key = 'paygate_id';
            } 

            $pgId->value = $request->paygate_id;
            $pgId->save();



            if(! ($pgKey = Settings::where('key', 'paygate_encryption_key')->first()) ) {
                $pgKey = new Settings;
                $pgKey->key = 'paygate_encryption_key';
            }

            $pgKey->value = $request->paygate_encryption_key;
            $pgKey->save();



        } 
        // if paygate settings not in request delete PayGate payment mode
        else {
            Settings::where('key', 'paygate')->forceDelete();
            Settings::where('key', 'paygate_id')->forceDelete();
            Settings::where('key', 'paygate_encryption_key')->forceDelete();
        }

        /* end of PayGate settings */




        $settings = Settings::all();



        
        foreach ($settings as $setting) {
            $key = $setting->key;

                $temp_setting = Settings::find($setting->id);

                if($temp_setting->key == 'site_icon'){
                    $site_icon = $request->file('site_icon');
                    if($site_icon == null)
                    {

                        $icon = $temp_setting->value;
                    }
                    else
                    {

                        $icon = Helper::upload_picture($site_icon);

                    }
                    $temp_setting->value = $icon;
                    $temp_setting->save();
                }

               else if($temp_setting->key == 'site_logo'){
                    $picture = $request->file('picture');
                    if($picture == null){
                    $logo = $temp_setting->value;
                    }
                    else
                    {
                        $logo = Helper::upload_picture($picture);
                    }
                    $temp_setting->value = $logo;
                    $temp_setting->save();
                }
                else if($temp_setting->key == 'mail_logo'){
                    $picture = $request->file('email_logo');
                    if($picture == null){
                    $logo = $temp_setting->value;
                    }
                    else
                    {
                        $logo = Helper::upload_picture($picture);
                    }
                    $temp_setting->value = $logo;
                    $temp_setting->save();
                }

                 else if($temp_setting->key == 'wallet_bay_key'){
                      $temp_setting->value = $request->wallet_bay_key;
                      $temp_setting->save();
                  }
                  else if($temp_setting->key == 'wallet_url'){
                      $temp_setting->value = $request->wallet_url;
                      $temp_setting->save();
                  }
                  else if($temp_setting->key == 'card'){
                      if($request->$key==1)
                      {
                          $temp_setting->value   = 1;

                      }
                      else
                      {
                          $temp_setting->value = 0;
                      }
                      $temp_setting->save();
                  }
                 else if($temp_setting->key == 'paypal'){
                      if($request->$key==1)
                      {
                          $temp_setting->value   = 1;
                      }
                      else
                      {
                          $temp_setting->value = 0;
                      }
                      $temp_setting->save();
                  }
                  else if($temp_setting->key == 'manual_request'){
                      if($request->$key==1)
                      {
                          $temp_setting->value   = 1;
                      }
                      else
                      {
                          $temp_setting->value = 0;
                      }
                      $temp_setting->save();
                  }
                  else if($request->$key!=''){
                $temp_setting->value = $request->$key;
                $temp_setting->save();
                }
            }
        return back()->with('setting', $settings)->with('flash_success','Settings Updated Successfully');
    }

    //Documents

    public function documents()
    {
        $document = Document::orderBy('created_at' , 'asc')->get();
        return view('admin.documents')->with('documents',$document)
                    ->withPage('documents')->with('sub_page','view-document');
    }

    public function document_edit(Request $request)
    {
        $document = Document::find($request->id);
        return view('admin.add-documents')->with('name', 'Edit Document')->with('document',$document)
                    ->withPage('documents')->with('sub_page','view-document');
    }

    public function add_document()
    {
        return view('admin.add-documents')->withPage('documents')->with('sub_page','add-document');
    }

    public function add_document_process(Request $request)
    {

                $validator = Validator::make(
                    $request->all(),
                    array(
                        'document_name' => 'required|max:255',

                    )
                );
            if($validator->fails())
        {
            $error_messages = implode(',', $validator->messages()->all());
            return back()->with('flash_errors', $error_messages);
        }
        else
        {
            if($request->id != '')
            {
                $document = Document::find($request->id);
                $message = tr('admin_not_doc_updated');
            }
            else
            {
                $document = new Document;
                $message = tr('admin_not_doc');
            }
                $document->name = $request->document_name;
                $document->save();

        if($document)
        {
            return back()->with('flash_success',$message);
        }
        else
        {
            return back()->with('flash_error',tr('admin_not_error'));
        }
        }
    }

    public function delete_document(Request $request)
    {

        $document = Document::find($request->id)->delete();

        if($document)
        {
            return back()->with('flash_success',tr('admin_not_doc_del'));
        }
        else
        {
            return back()->with('flash_error',tr('admin_not_error'));
        }
    }
    
    //Currency

    public function currency()
    {
        $currency = Currency::orderBy('created_at' , 'asc')->get();
        return view('admin.currency')->with('currency',$currency)
                    ->withPage('currency')->with('sub_page','view-currency');
    }

    public function currency_edit(Request $request)
    {
        $currency = Currency::find($request->id);
        return view('admin.add-currency')->with('name', 'Edit Currency')->with('currency',$currency)
                    ->withPage('currency')->with('sub_page','view-currency');
    }

    public function add_currency()
    {
        return view('admin.add-currency')->withPage('currency')->with('sub_page','add-currency');
    }

    public function add_currency_process(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            array(
                'currency_name' => 'required',
                'currency_value' => 'required|numeric'

            )
        );
            if($validator->fails())
        {
            $error_messages = implode(',', $validator->messages()->all());
            return back()->with('flash_errors', $error_messages);
        }
        else
        {
            if($request->id != '')
            {
                $currency = Currency::find($request->id);
                $message = tr('admin_not_currency_updated');
            }
            else
            {
                $currency = new Currency;
                $message = tr('admin_not_currency');
            }
                $currency->currency_name = $request->currency_name;
                $currency->currency_value = $request->currency_value;
                $currency->save();

        if($currency)
        {
            return back()->with('flash_success',$message);
        }
        else
        {
            return back()->with('flash_error',tr('admin_not_error'));
        }
        }
    }

    public function delete_currency(Request $request)
    {

        $document = Currency::find($request->id)->delete();

        if($document)
        {
            return back()->with('flash_success',tr('admin_not_currency_del'));
        }
        else
        {
            return back()->with('flash_error',tr('admin_not_error'));
        }
    }

    //Service Types

    public function service_types()
    {
        $service = ServiceType::orderBy('order' , 'asc')->get();
        return view('admin.service_types')->with('services',$service)->withPage('service_types')->with('sub_page','view-service');
    }

    public function edit_service(Request $request)
    {
        $service = ServiceType::find($request->id);
        return view('admin.add_service_types')->with('name', 'Edit Service Types')->with('service',$service)
                    ->withPage('service_types')->with('sub_page','view-service');
    }

    public function add_service_type()
    {
        return view('admin.add_service_types')->withPage('service_types')->with('sub_page','add-service');
    }

    public function add_service_process(Request $request)
    {
      $picture = $request->file('picture');
      if($request->id != '')
      {
          $validator = Validator::make(
              $request->all(),
              array(
                  'service_name' => 'required|max:255',
                  'number_seat' => 'required|max:10',
                  'base_fare' => 'required',
                  'min_fare' => 'required',
                  'tax_fee' => 'required',
                  'booking_fee' => 'required',
                  'price_per_min' => 'required',
                  'price_per_unit_distance' => 'required',
                  'distance_unit' => 'required|in:'.UNIT_DISTANCE,

              )
          );
      }
      else {
        $validator = Validator::make(
            $request->all(),
            array(
                'service_name' => 'required|max:255',
                // 'provider_name' => 'required|max:255',
                'base_fare' => 'required',
                'min_fare' => 'required',
                'tax_fee' => 'required',
                'booking_fee' => 'required',
                'number_seat' => 'required|max:10',
                'price_per_min' => 'required',
                'price_per_unit_distance' => 'required',
                'distance_unit' => 'required|in:'.UNIT_DISTANCE,
                'picture' => 'required|mimes:jpeg,jpg,bmp,png',

            )
        );
      }
        if($validator->fails())
        {
            $error_messages = implode(',', $validator->messages()->all());
            return back()->with('flash_errors', $error_messages);
        }
        else
        {
            if($request->id != '')
            {
                $service = ServiceType::find($request->id);
                if($picture != ''){
                $service->picture = Helper::upload_picture($picture);
                }
                $message = tr('admin_not_st_updated');
            }
            else
            {
                $service = new ServiceType;
                $service->picture = Helper::upload_picture($picture);
                $message = tr('admin_not_st');

            }
            if ($request->is_default == 1) {
            ServiceType::where('status', 1)->update(array('status' => 0));
            $service->status = 1;
            }
            else
            {
                $service->status = 0;
            }
            $service->name = $request->service_name;
            // $service->provider_name = $request->provider_name;
            $service->number_seat = $request->number_seat;
            $service->price_per_min = $request->price_per_min;
            $service->price_per_unit_distance = $request->price_per_unit_distance;
            $service->distance_unit = $request->distance_unit;
            $service->base_fare = $request->base_fare;
            $service->min_fare = $request->min_fare;
            $service->tax_fee = $request->tax_fee;
            $service->booking_fee = $request->booking_fee;
            $service->save();

        if($service)
        {
            return back()->with('flash_success',$message);
        }
        else
        {
            return back()->with('flash_error',tr('admin_not_error'));
        }
        }
    }

    public function delete_service(Request $request)
    {

        $service = ServiceType::find($request->id)->delete();

        if($service)
        {
            return back()->with('flash_success',tr('admin_not_st_del'));
        }
        else
        {
            return back()->with('flash_error',"Something went Wrong");
        }
    }


    //Hourly package Types

    public function hourly_packages()
    {
        $hourly_package = DB::table('hourly_packages')->leftJoin('service_types','hourly_packages.car_type_id','=','service_types.id')
                        ->select('hourly_packages.id','hourly_packages.distance','hourly_packages.price',
                            'hourly_packages.number_hours','hourly_packages.car_type_id','service_types.name')->get();
        return view('admin.hourly_packages')->with('hourly_packages',$hourly_package)->withPage('hourly_packages')->with('sub_page','view-hourly_package');
    }

    public function edit_hourly_package(Request $request)
    {
        $hourly_package = HourlyPackage::find($request->id);
        if($hourly_package)
            $provider_type = $hourly_package->car_type_id;
        else
            $provider_type = "";
        $service_type = ServiceType::all();
        return view('admin.add_hourly_package')->with('name', 'Edit Hourly Package')->with('hourly_package',$hourly_package)
                    ->withPage('hourly_packages')->with('sub_page','view-hourly_package')->with('provider_type',$provider_type)
                    ->with('service_types',$service_type);
    }

    public function add_hourly_package()
    {
        $service_type = ServiceType::all();
        return view('admin.add_hourly_package')->withPage('hourly_packages')->with('sub_page','add-hourly_package')
                        ->with('service_types',$service_type);
    }

    public function add_hourly_package_process(Request $request)
    {
      if($request->id != '')
      {
          $validator = Validator::make(
              $request->all(),
              array(
                  'number_hours' => 'required|max:255',
                  'price' => 'required|max:10',
                  'distance' => 'required',
                  'service_type' => 'required',

              )
          );
      }
      else {
        $validator = Validator::make(
            $request->all(),
            array(
                'number_hours' => 'required|max:255',
                'price' => 'required|max:10',
                'distance' => 'required',
                'service_type' => 'required',
            )
        );
      }
        if($validator->fails())
        {
            $error_messages = implode(',', $validator->messages()->all());
            return back()->with('flash_errors', $error_messages);
        }
        else
        {
            if($request->id != '')
            {
                $hourly_package = HourlyPackage::find($request->id);
                
                $message = tr('admin_not_st_updated');
            }
            else
            {
                $hourly_package = new HourlyPackage;
                $message = tr('admin_not_st');

            }

            $hourly_package->number_hours = $request->number_hours;
            $hourly_package->price = $request->price;
            $hourly_package->distance = $request->distance;
            $hourly_package->car_type_id = $request->service_type;
            $hourly_package->save();

        if($hourly_package)
        {
            return back()->with('flash_success',$message);
        }
        else
        {
            return back()->with('flash_error',tr('admin_not_error'));
        }
        }
    }

    public function delete_hourly_package(Request $request)
    {

        $hourly_package = HourlyPackage::find($request->id)->delete();

        if($hourly_package)
        {
            return back()->with('flash_success',tr('admin_not_st_del'));
        }
        else
        {
            return back()->with('flash_error',"Something went Wrong");
        }
    }


    
  // Airport Details

    public function airport_details()
    {
        $airport_detail = AirportDetail::all();
        return view('admin.airport.airport_details')->with('airport_details',$airport_detail)->withPage('airport')->with('sub_page','airport-details');
    }

    public function edit_airport_detail(Request $request)
    {
        $airport_detail = AirportDetail::find($request->id);
        // $key = "AIzaSyAyw6xHTxm2Ym4yRZmzfOntxYcx-_M5tfg";
        $key = Helper::getKey();
        return view('admin.airport.add_airport_detail')->with('name', 'Edit Airport Details')->with('airport_detail',$airport_detail)
                    ->withPage('airport')->with('sub_page','airport-detail')
                    ->with('key', $key);
    }

    public function add_airport_detail()
    {
        $key = Helper::getKey();
        // echo "string".$key; exit;
        return view('admin.airport.add_airport_detail')->withKey($key)->withPage('airport')->with('sub_page','airport-detail');
    }

    public function add_airport_detail_process(Request $request)
    {
        $validator = Validator::make(
          $request->all(),
          array(
              'name' => 'required|max:255',
              'zipcode' => 'required|max:255',
            )
        );
  
        if($validator->fails())
        {
            $error_messages = implode(',', $validator->messages()->all());
            return back()->with('flash_errors', $error_messages);
        }
        else
        {
            if($request->id != '')
            {
                $airport_detail = AirportDetail::find($request->id);
                
                $message = tr('admin_not_st_updated');
            }
            else
            {
                $airport_detail = new AirportDetail;
                $message = tr('admin_not_st');

            }

            $airport_detail->name = $request->name;
            $airport_detail->zipcode = $request->zipcode;
            $airport_detail->save();

        if($airport_detail)
        {
            return back()->with('flash_success',$message);
        }
        else
        {
            return back()->with('flash_error',tr('admin_not_error'));
        }
        }
    }

    public function delete_airport_detail(Request $request)
    {

        $airport_detail = AirportDetail::find($request->id)->delete();

        if($airport_detail)
        {
            return back()->with('flash_success',tr('admin_not_st_del'));
        }
        else
        {
            return back()->with('flash_error',"Something went Wrong");
        }
    }

    //Location Details

    public function location_details()
    {
        $location_detail = LocationDetail::all();
        return view('admin.airport.location_details')->with('location_details',$location_detail)->withPage('airport')->with('sub_page','location-details');
    }

    public function edit_location_detail(Request $request)
    {
        $location_detail = LocationDetail::find($request->id);
        $key = Helper::getKey();
        return view('admin.airport.add_location_detail')->with('name', 'Edit Location Details')->withKey($key)->with('location_detail',$location_detail)
                    ->withPage('airport')->with('sub_page','location-details');
    }

    public function add_location_detail()
    {
        $key = Helper::getKey();
        return view('admin.airport.add_location_detail')->withPage('airport')->withKey($key)->with('sub_page','location-details');
    }

    public function add_location_detail_process(Request $request)
    {
        $validator = Validator::make(
          $request->all(),
          array(
              'name' => 'required|max:255',
              'zipcode' => 'required|max:255',
            )
        );
  
        if($validator->fails())
        {
            $error_messages = implode(',', $validator->messages()->all());
            return back()->with('flash_errors', $error_messages);
        }
        else
        {
            if($request->id != '')
            {
                $location_detail = LocationDetail::find($request->id);
                
                $message = tr('admin_not_st_updated');
            }
            else
            {
                $location_detail = new LocationDetail;
                $message = tr('admin_not_st');

            }

            $location_detail->name = $request->name;
            $location_detail->zipcode = $request->zipcode;
            $location_detail->save();

        if($location_detail)
        {
            return back()->with('flash_success',$message);
        }
        else
        {
            return back()->with('flash_error',tr('admin_not_error'));
        }
        }
    }

    public function delete_location_detail(Request $request)
    {

        $location_detail = LocationDetail::find($request->id)->delete();

        if($location_detail)
        {
            return back()->with('flash_success',tr('admin_not_st_del'));
        }
        else
        {
            return back()->with('flash_error',"Something went Wrong");
        }
    }

    //Airport Pricing Details

    public function airport_pricings()
    {
        $airport_pricing = AirportPrice::all();
        // echo count($airport_pricing); exit;

        return view('admin.airport.airport_price')->with('airport_pricings',$airport_pricing)
            ->withPage('airport')->with('sub_page','airport-pricing');
    }

    public function edit_airport_pricing(Request $request)
    {
        $airport_pricing = AirportPrice::find($request->id);
        $airport_details = AirportDetail::all();
        $location_details = LocationDetail::all();
        $service_types = ServiceType::all();
        return view('admin.airport.add_airport_price')->with('name', 'Edit Airport Pricing')->with('airport_price',$airport_pricing)
                    ->with('airport_details',$airport_details)->with('location_details',$location_details)
                    ->with('service_types',$service_types)
                    ->withPage('airport')->with('sub_page','airport-pricing');
    }

    public function add_airport_pricing()
    {
        $airport_details = AirportDetail::all();
        $location_details = LocationDetail::all();
        $service_types = ServiceType::all();
        return view('admin.airport.add_airport_price')
                ->with('airport_details',$airport_details)->with('location_details',$location_details)
                ->with('service_types',$service_types)
                ->withPage('airport')->with('sub_page','airport-pricing');
    }

    public function add_airport_pricing_process(Request $request)
    {
        $validator = Validator::make(
          $request->all(),
          array(
              'airport_detail' => 'required|exists:airport_details,id',
              'location_detail' => 'required|exists:location_details,id',
              'service_type' => 'required|exists:service_types,id',
              'price' => 'required',
              'number_tolls' => 'numeric',
            )
        );
  
        if($validator->fails())
        {
            $error_messages = implode(',', $validator->messages()->all());
            return back()->with('flash_errors', $error_messages);
        }
        else
        {
            if($request->id != '')
            {
                $airport_pricing = AirportPrice::find($request->id);
                
                $message = tr('admin_not_st_updated');
            }
            else
            {
                $airport_pricing = new AirportPrice;
                $message = tr('admin_not_st');

            }

            $airport_pricing->airport_details_id = $request->airport_detail;
            $airport_pricing->location_details_id = $request->location_detail;
            $airport_pricing->service_type_id = $request->service_type;
            $airport_pricing->number_tolls = $request->number_tolls;
            $airport_pricing->price = $request->price;
            $airport_pricing->save();

        if($airport_pricing)
        {
            return back()->with('flash_success',$message);
        }
        else
        {
            return back()->with('flash_error',tr('admin_not_error'));
        }
        }
    }

    public function delete_airport_pricing(Request $request)
    {

        $airport_pricing = AirportPrice::find($request->id)->delete();

        if($airport_pricing)
        {
            return back()->with('flash_success',tr('admin_not_st_del'));
        }
        else
        {
            return back()->with('flash_error',"Something went Wrong");
        }
    }

    public function provider_reviews()
    {
            $provider_reviews = DB::table('provider_ratings')
                ->leftJoin('providers', 'provider_ratings.provider_id', '=', 'providers.id')
                ->leftJoin('users', 'provider_ratings.user_id', '=', 'users.id')
                ->select('provider_ratings.id as review_id', 'provider_ratings.rating', 'provider_ratings.comment', 'users.first_name as user_first_name', 'users.last_name as user_last_name', 'providers.first_name as provider_first_name', 'providers.last_name as provider_last_name', 'users.id as user_id', 'providers.id as provider_id', 'provider_ratings.created_at')
                ->orderBy('provider_ratings.id', 'DESC')
                ->get();


            return view('admin.reviews')
                        ->with('reviews', $provider_reviews)
                        ->withPage('rating_review')->with('sub_page','provider-review');
    }

    public function user_reviews()
    {

            $user_reviews = DB::table('user_ratings')
                ->leftJoin('providers', 'user_ratings.provider_id', '=', 'providers.id')
                ->leftJoin('users', 'user_ratings.user_id', '=', 'users.id')
                ->select('user_ratings.id as review_id', 'user_ratings.rating', 'user_ratings.comment', 'users.first_name as user_first_name', 'users.last_name as user_last_name', 'providers.first_name as provider_first_name', 'providers.last_name as provider_last_name', 'users.id as user_id', 'providers.id as provider_id', 'user_ratings.created_at')
                ->orderBy('user_ratings.id', 'ASC')
                ->get();
            return view('admin.reviews')->with('name', 'User')->with('reviews', $user_reviews)
                      ->withPage('rating_review')->with('sub_page','user-review');;
    }

    public function delete_user_reviews(Request $request) {
        $user = UserRating::find($request->id)->delete();
        return back()->with('flash_success', tr('admin_not_ur_del'));
    }

    public function delete_provider_reviews(Request $request) {
        $provider = ProviderRating::find($request->id)->delete();
        return back()->with('flash_success', tr('admin_not_pr_del'));
    }

    public function user_history(Request $request)
    {
        $requests = DB::table('requests')
                ->Where('user_id',$request->id)
                ->leftJoin('providers', 'requests.confirmed_provider', '=', 'providers.id')
                ->leftJoin('users', 'requests.user_id', '=', 'users.id')
                ->leftJoin('request_payments', 'requests.id', '=', 'request_payments.request_id')
                ->select('users.first_name as user_first_name', 'users.last_name as user_last_name', 'providers.first_name as provider_first_name', 'providers.last_name as provider_last_name', 'users.id as user_id', 'providers.id as provider_id', 'requests.is_paid',  'requests.id as id', 'requests.created_at as date', 'requests.confirmed_provider', 'requests.status', 'requests.provider_status', 'requests.amount', 'request_payments.payment_mode as payment_mode', 'request_payments.status as payment_status')
                ->orderBy('requests.created_at', 'ASC')
                ->get();
        return view('admin.request')->with('requests', $requests)->withPage('users')->with('sub_page','view-user')
              ->with('name','view_user_history');
    }

    public function provider_history(Request $request)
    {
        $requests = DB::table('requests')
                ->Where('confirmed_provider',$request->id)
                ->leftJoin('providers', 'requests.confirmed_provider', '=', 'providers.id')
                ->leftJoin('users', 'requests.user_id', '=', 'users.id')
                ->leftJoin('request_payments', 'requests.id', '=', 'request_payments.request_id')
                ->select('users.first_name as user_first_name', 'users.last_name as user_last_name', 'providers.first_name as provider_first_name', 'providers.last_name as provider_last_name', 'users.id as user_id', 'providers.id as provider_id', 'requests.is_paid',  'requests.id as id', 'requests.created_at as date', 'requests.confirmed_provider', 'requests.status', 'requests.provider_status', 'requests.amount', 'request_payments.payment_mode as payment_mode', 'request_payments.status as payment_status')
                ->orderBy('requests.created_at', 'DESC')
                ->get();
        return view('admin.request')->with('requests', $requests)->withPage('providers')->with('sub_page','view-provider')
              ->with('name','view_provider_history');
    }

    public function requests()
    {
        $requests = DB::table('requests')
                ->leftJoin('providers', 'requests.confirmed_provider', '=', 'providers.id')
                ->leftJoin('users', 'requests.user_id', '=', 'users.id')
                ->leftJoin('request_payments', 'requests.id', '=', 'request_payments.request_id')
                ->select('users.first_name as user_first_name', 'users.last_name as user_last_name', 'providers.first_name as provider_first_name', 'providers.last_name as provider_last_name', 'users.id as user_id', 'providers.id as provider_id', 'requests.is_paid',  'requests.id as id', 'requests.created_at as date', 'requests.confirmed_provider', 'requests.status', 'requests.provider_status', 'request_payments.total as amount', 'request_payments.payment_mode as payment_mode', 'request_payments.status as payment_status')
                ->orderBy('requests.created_at', 'desc')
                ->get();
        return view('admin.request')->with('requests', $requests)->withPage('requests')->with('sub_page','');
    }
    public function view_request(Request $request)
    {

        $requests = DB::table('requests')
                ->where('requests.id',$request->id)
                ->leftJoin('providers', 'requests.confirmed_provider', '=', 'providers.id')
                ->leftJoin('users', 'requests.user_id', '=', 'users.id')
                ->leftJoin('request_payments', 'requests.id', '=', 'request_payments.request_id')
                ->select('users.first_name as user_first_name', 'users.last_name as user_last_name', 'providers.first_name as provider_first_name', 'providers.last_name as provider_last_name', 'users.id as user_id', 'providers.id as provider_id', 'requests.is_paid',  'requests.id as id', 'requests.created_at as date', 'requests.confirmed_provider', 'requests.status', 'requests.provider_status', 'requests.amount', 'request_payments.payment_mode as payment_mode', 'request_payments.status as payment_status', 'request_payments.total_time as total_time','request_payments.base_price as base_price', 'request_payments.time_price as time_price', 'request_payments.tax_price as tax', 'request_payments.total as total_amount', 'requests.s_latitude as latitude', 'requests.s_longitude as longitude','requests.start_time','requests.end_time','requests.s_address as request_address','requests.before_image' , 'requests.after_image')
                ->first();
        return view('admin.request-view')->with('page' ,'requests')->with('sub-page' , "")->with('request', $requests);
    }

    public function mapview()
    {
        // dd(\Auth::guard('admin')->user());
        $Providers = Provider::all();
        $page = 'maps';
        $sub_page = 'provider-map';
        return view('admin.map', compact('Providers','page','sub_page'));
    }

    public function usermapview()
    {
        // dd(\Auth::guard('admin')->user());
        $Users = User::where('latitude', '!=', '0')->where('longitude', '!=', '0')->get();
        $page = 'maps';
        $sub_page = 'user-map';
        return view('admin.user-map', compact('Users','page','sub_page'));

    }

    public function help()
    {
        return view('admin.help');
    }

    public function provider_details(Request $request)
    {
        $provider = Provider::find($request->id);
        $avg_rev = ProviderRating::where('provider_id',$request->id)->avg('rating');


        if($provider) {
            $service = "";
            $service_type = ProviderService::where('provider_id' ,$provider->id)
                                ->leftJoin('service_types' ,'provider_services.service_type_id','=' , 'service_types.id')
                                ->first();
            if($service_type) {
                $service = $service_type->name;
            }
            return view('admin.provider-details')->with('provider' , $provider)->withService($service)->with('review',$avg_rev)
                      ->withPage('maps')->with('sub_page','provider-map');
        } else {
            return back()->with('error' , "Provider details not found");
        }
    }

    public function user_details(Request $request)
    {
        $user = User::find($request->id);
        $avg_rev = UserRating::where('user_id',$request->id)->avg('rating');

        if($user) {
          if($request->option !='')
            return view('admin.user-details')->with('user' , $user)->with('review',$avg_rev)
                      ->withPage('users')->with('sub_page','view-user')->with('name',$request->option);
          else
            return view('admin.user-details')->with('user' , $user)->with('review',$avg_rev)
                        ->withPage('maps')->with('sub_page','user-map');
        } else {
            return back()->with('error' , "User details not found");
        }
    }

    public function promo_codes()
    {
        $promo_codes = DB::table('promo_codes')->get();
        return view('admin.promo_codes')->with('promocodes',$promo_codes)
                                      ->withPage('promo_codes')->with('sub_page','view-promocode');
    }

    public function add_promo_code()
    {
        return view('admin.add_promo_code')->withPage('promo_codes')->with('sub_page','add-promocode');
    }

    public function edit_promo_code(Request $request)
    {
        $promo_code = PromoCode::find($request->id);
        return view('admin.add_promo_code')->withPage('promo_codes')->with('sub_page','add-promocode')->with('promo_code',$promo_code);
    }

    public function add_promo_code_process(Request $request)
    {
      if($request->id != '')
      {
          $validator = Validator::make(
              $request->all(),
              array(
                  'start_date' => 'required',
                  'start_time' => 'required',
                  'end_date' => 'required',
                  'end_time' => 'required',
                  'short_description' => 'required|max:100',
                  'long_description' => 'required',
                  // 'coupon_code' => 'required',
                  'value' => 'required',
                  'max_promo' => 'required',
                  'max_usage' => 'required',
              )
          );
      }
      else {
        $validator = Validator::make(
              $request->all(),
              array(
                  'start_date' => 'required',
                  'start_time' => 'required',
                  'end_date' => 'required',
                  'end_time' => 'required',
                  'short_description' => 'required|max:100',
                  'long_description' => 'required',
                  // 'coupon_code' => 'required',
                  'value' => 'required',
                  'max_promo' => 'required',
                  'max_usage' => 'required',
              )
          );
      }
        if($validator->fails())
        {
            $error_messages = implode(',', $validator->messages()->all());
            return back()->with('flash_errors', $error_messages);
        }
        else
        {
            if($request->id != '')
            {
                $promo_codes = PromoCode::find($request->id);
                $message = tr('admin_not_st_updated');
            }
            else
            {
                $promo_codes = new PromoCode;
                $message = "Promo code has been added successfully";

            }
            $start_time = explode(' ', $request->start_time);
            $start = $request->start_date." ".$start_time[0]; 

            $end_time = explode(' ', $request->end_time);
            $end = $request->end_date." ".$end_time[0]; 

            $promo_codes->scope = $request->scope;
            $promo_codes->coupon_code = $request->coupon_code;
            $promo_codes->value = $request->value;
            $promo_codes->type = $request->type;
            $promo_codes->start = $start;
            $promo_codes->end = $end;
            $promo_codes->short_description = $request->short_description;
            $promo_codes->long_description = $request->long_description;
            $promo_codes->max_promo = $request->max_promo;
            $promo_codes->max_usage = $request->max_usage;
            $promo_codes->save();

        if($promo_codes)
        {
            return back()->with('flash_success',$message);
        }
        else
        {
            return back()->with('flash_error',tr('admin_not_error'));
        }
        }
    }

    public function delete_promo_code(Request $request)
    {

        if($provider = PromoCode::find($request->id))
        {

            $provider = PromoCode::find($request->id)->delete();
        }

        if($provider)
        {
            return back()->with('flash_success', 'Promo Code Has Been Deleted Successfully.');
        }
        else
        {
            return back()->with('flash_error',tr('admin_not_error'));
        }
    }

    public function push_notifications(Request $request)
    {
        $result = User::where("device_token", "!=", "")->where("device_type", "!=", "")->get();
        return view('admin.push_notifications')->withResult($result)->withPage('push_notifications')->with('sub_page','view-push_notifications');
    }

    public function mass_push_notification_send(Request $request)
    {
        $type = $request->type;
        if($type == "users")
        {
            $user_type  = 1;
        }
        else if($type == "providers")
        {
            $user_type = 2;
        }
        $numbers      = $request->numbers;
        $push_title      = $request->push_title;
        $push_message      = $request->push_message;
        // $push_message    = strip_tags($push_message); 
            $validator = Validator::make(
                array(
                    'push_title' => $push_title,
                    'push_message' => $push_message,
                    'numbers' => $numbers,
                ), array(
                    'push_title' => 'required',
                    'numbers' => 'required',
                    'push_message' => 'required'
                )
            );

            if ($validator->fails()) {
                $error_messages = implode(',', $validator->messages()->all());
                return back()->with('flash_errors', $error_messages);
            }
            else
            {
                // echo count($numbers); exit;
                foreach($numbers as $id){
                    Helper::send_notifications($id, $user_type, $push_title, $push_message );
                }
                return back()->with('flash_success', 'Push notification sent successfully.');
            }
    }


}

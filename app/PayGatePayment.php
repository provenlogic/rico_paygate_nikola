<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PayGatePayment extends Model
{
    protected $table = 'paygate_payments';
    protected $fillable = [
        'request_id', 
        'reference_id',
        'data',
        'status',
    ];

    public function getTableName()
    {
        return $this->table;
    }



}
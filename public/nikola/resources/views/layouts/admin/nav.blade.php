<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="@if(Auth::guard('admin')->user()->picture){{Auth::guard('admin')->user()->picture}} @else {{asset('admin-css/dist/img/avatar.png')}} @endif" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{Auth::guard('admin')->user()->name}}</p>
                <a href="{{route('admin.profile')}}">{{ tr('admin') }}</a>
            </div>
        </div>

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">

            <li id="dashboard">
              <a href="{{route('admin.dashboard')}}">
                <i class="fa fa-dashboard"></i> <span>{{tr('dashboard')}}</span>
              </a>

            </li>

            <li class="treeview" id="maps">

                <a href="#">
                    <i class="fa fa-map"></i> <span>{{tr('map')}}</span> <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu">
                    <li id="user-map"><a href="{{route('admin.usermapview')}}"><i class="fa fa-circle-o"></i>Booking Stats</a></li>
                    <li id="provider-map"><a href="{{route('admin.mapview')}}"><i class="fa fa-circle-o"></i>Driver availability Stats </a></li>
                </ul>

            </li>

            <li class="treeview" id="corporates">

                <a href="#">
                    <i class="fa fa-users"></i> <span>Corporate's under you</span> <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu">
                    <li id="add-corporate"><a href="{{route('admin.add.corporate')}}"><i class="fa fa-circle-o"></i>Add a Corporate</a></li>
                    <li id="view-corporate"><a href="{{route('admin.corporates')}}"><i class="fa fa-circle-o"></i>{{tr('view_corporates')}}</a></li>
                </ul>

            </li>

            <li class="treeview" id="users">

                <a href="#">
                    <i class="fa fa-user"></i> <span>Passenger Management</span> <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu">
                    <li id="add-user"><a href="{{route('admin.add.user')}}"><i class="fa fa-circle-o"></i>Add a passenger</a></li>
                    <li id="view-user"><a href="{{route('admin.users')}}"><i class="fa fa-circle-o"></i>View all Passengers</a></li>
                </ul>

            </li>

            <li class="treeview" id="providers">

                <a href="#">
                    <i class="fa fa-users"></i> <span>Driver Management</span> <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu">
                    <li id="add-provider"><a href="{{route('admin.add.provider')}}"><i class="fa fa-circle-o"></i>Add a Driver</a></li>
                    <li id="view-provider"><a href="{{route('admin.providers')}}"><i class="fa fa-circle-o"></i>View all Drivers</a></li>
                </ul>

            </li>

            <li id="requests">
                <a href="{{route('admin.requests')}}">
                    <i class="fa fa-credit-card"></i> <span>Ride Requests Management</span>
                </a>
            </li>

            <li class="treeview" id="service_types">

                <a href="#">
                    <i class="fa fa-users"></i> <span>{{tr('service_types')}}</span> <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu">
                    <li id="add-service"><a href="{{route('admin.add.service.type')}}"><i class="fa fa-circle-o"></i>Add a Vehicle Type</a></li>
                    <li id="view-service"><a href="{{route('admin.service.types')}}"><i class="fa fa-circle-o"></i>View all Vehicle Types</a></li>
                </ul>

            </li>

            <li class="treeview" id="promo_codes">

                <a href="#">
                    <i class="fa fa-users"></i> <span>Promo Codes</span> <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu">
                    <li id="add-promocode"><a href="{{route('admin.add.promo_code')}}"><i class="fa fa-circle-o"></i>Add a Promo Code</a></li>
                    <li id="view-promocode"><a href="{{route('admin.promo_codes')}}"><i class="fa fa-circle-o"></i>View all Promo Codes</a></li>
                </ul>

            </li>

            <li class="treeview" id="hourly_packages">

                <a href="#">
                    <i class="fa fa-users"></i> <span>Rentals Management</span> <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu">
                    <li id="add-hourly_package"><a href="{{route('admin.add.hourly_package')}}"><i class="fa fa-circle-o"></i>Add a Hourly Package</a></li>
                    <li id="view-hourly_package"><a href="{{route('admin.hourly_package')}}"><i class="fa fa-circle-o"></i>View all the packages</a></li>
                </ul>

            </li>

          <!--    <li class="treeview" id="airport">
              <a href="#">
                <i class="fa fa-share"></i> <span>Airport Package</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu menu-open" style="display: block;">
                <li id="airport-details">
                  <a href="#"><i class="fa fa-circle-o"></i> Airport Details
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu menu-open" style="display: none;">
                    <li><a href="{{route('admin.airport_detail.add')}}"><i class="fa fa-circle-o"></i> Add Airport Detail</a></li>
                    <li><a href="{{route('admin.airport_details')}}"><i class="fa fa-circle-o"></i> View Airport Detail</a></li>

                  </ul>
                </li>
                <li id="location-details">
                  <a href="#"><i class="fa fa-circle-o"></i> Location Details
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu menu-open" style="display: none;">
                    <li><a href="{{route('admin.location_detail.add')}}"><i class="fa fa-circle-o"></i> Add Location Detail</a></li>
                    <li><a href="{{route('admin.location_details')}}"><i class="fa fa-circle-o"></i> View Location Detail</a></li>

                  </ul>
                </li>
                <li id="airport-pricing">
                  <a href="#"><i class="fa fa-circle-o"></i> Airport Pricing
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu menu-open" style="display: none;">
                    <li><a href="{{route('admin.airport_pricing.add')}}"><i class="fa fa-circle-o"></i> Add Airport Price</a></li>
                    <li><a href="{{route('admin.airport_pricings')}}"><i class="fa fa-circle-o"></i> View Airport Price</a></li>

                  </ul>
                </li>
              </ul>
            </li> -->
            <li class="treeview " id="airport" >
                  <a href="#"><i class="fa fa-circle-o"></i> Airport rides
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu" style="display: none;">
                    <li id="airport-details" class="active">
                      <a href="#"><i class="fa fa-circle-o"></i> Airport Details
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                      </a>
                      <ul class="treeview-menu active">
                        <li class="active" ><a href="{{route('admin.airport_detail.add')}}"><i class="fa fa-circle-o"></i> Add an Airport</a></li>
                        <li><a href="{{route('admin.airport_details')}}"><i class="fa fa-circle-o"></i> View all Airports</a></li>
                      </ul>
                    </li>
                    <li id="location-details">
                      <a href="#"><i class="fa fa-circle-o"></i> Destination details
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                      </a>
                      <ul class="treeview-menu">
                        <li><a href="{{route('admin.location_detail.add')}}"><i class="fa fa-circle-o"></i>Add a destination</a></li>
                        <li><a href="{{route('admin.location_details')}}"><i class="fa fa-circle-o"></i>View all Destination's</a></li>
                      </ul>
                    </li>
                    <li id="airport-pricing">
                      <a href="#"><i class="fa fa-circle-o"></i> Pricing Management
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                      </a>
                      <ul class="treeview-menu">
                        <li><a href="{{route('admin.airport_pricing.add')}}"><i class="fa fa-circle-o"></i>Pricing setup</a></li>
                        <li><a href="{{route('admin.airport_pricings')}}"><i class="fa fa-circle-o"></i>View all Pricing plans</a></li>
                      </ul>
                    </li>
                  </ul>
            </li>

            <!-- <li class="treeview">
              <a href="#">
                <i class="fa fa-share"></i> <span>Multilevel</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu menu-open" style="display: none;">
                <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
                <li>
                  <a href="#"><i class="fa fa-circle-o"></i> Level One
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Two</a></li>
                    <li>
                      <a href="#"><i class="fa fa-circle-o"></i> Level Two
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                      </a>
                      <ul class="treeview-menu">
                        <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                        <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                      </ul>
                    </li>
                  </ul>
                </li>
                <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
              </ul>
            </li>  -->

            <li class="treeview" id="rating_review">

                <a href="#">
                    <i class="fa fa-users"></i> <span>Ratings</span> <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu">
                    <li id="user-review"><a href="{{route('admin.user_reviews')}}"><i class="fa fa-circle-o"></i>Passenger Ratings</a></li>
                    <li id="provider-review"><a href="{{route('admin.provider_reviews')}}"><i class="fa fa-circle-o"></i>Driver Ratings</a></li>
                </ul>

            </li>

            <li class="treeview" id="documents">

                <a href="#">
                    <i class="fa fa-users"></i> <span>Documents Management</span> <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu">
                    <li id="add-document"><a href="{{route('admin.add_document')}}"><i class="fa fa-circle-o"></i>Add a Document Type</a></li>
                    <li id="view-document"><a href="{{route('admin.documents')}}"><i class="fa fa-circle-o"></i>View all Document Types</a></li>
                </ul>

            </li>

            <li class="treeview" id="currency">

                <a href="#">
                    <i class="fa fa-users"></i> <span>Currency Management</span> <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu">
                    <li id="add-currency"><a href="{{route('admin.add_currency')}}"><i class="fa fa-circle-o"></i>Add Currency</a></li>
                    <li id="view-currency"><a href="{{route('admin.currency')}}"><i class="fa fa-circle-o"></i>View all Currencies</a></li>
                </ul>

            </li>

            <li id="payments">
                <a href="{{route('admin.payments')}}">
                    <i class="fa fa-credit-card"></i> <span>Transactions</span>
                </a>
            </li>

            <li id="push_notifications">
                <a href="{{route('admin.push_notifications')}}">
                    <i class="fa fa-gears"></i> <span>Push Notifications</span>
                </a>
            </li>

            <li id="settings">
                <a href="{{route('admin.settings')}}">
                    <i class="fa fa-gears"></i> <span>{{tr('settings')}}</span>
                </a>
            </li>

            <li id="profile">
                <a href="{{route('admin.profile')}}">
                    <i class="fa fa-diamond"></i> <span>{{tr('account')}}</span>
                </a>
            </li>

            <li>
                <a href="{{route('admin.logout')}}">
                    <i class="fa fa-sign-out"></i> <span>{{tr('sign_out')}}</span>
                </a>
            </li>

        </ul>

    </section>

    <!-- /.sidebar -->

</aside>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>PayHost Payment Result</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <style type="text/css">
        	.header
        	{
				font-size: 21px;
			    font-weight: 700;
			    font-family: sans-serif;
			    padding-top: 10px;
			    padding-bottom: 10px;
        	}
        </style>
     
    </head>
    <body>
        <nav class="navbar navbar-default" 
        	style="background-color: #0c3d99;color: white;border-color: #0c3d99;border-radius: 0px;">
		  <div class="container-fluid">
		  	<div class="header">PayGate Payment Confirmation</div>
		  </div>
		</nav>
		<div class="row">
			<div class="col-md-3"></div>			
			<div class="col-md-6 col-sm-12 col-xs-12">

					@if($CHECKSUM != $generatedChecksum || $TRANSACTION_STATUS != 1)
					<div style="text-align: center;font-size: 32px;color: red;">
						<i class="fa fa-times-circle"></i>
						Failed !!
					</div>
					@else

					<div style="text-align: center;font-size: 32px;color: green;">
						<i class="fa fa-check-circle"></i>
						Success !!
					</div>
					@endif


					<div style="padding: 25px;">
						<label>Payment Details</label>
						<table class="table table-bordered">
						    <tbody>
						    	<tr>
						        <td>Name</td>
						        <td>{{$extra->name}}</td>
						      </tr>
						      <tr>
						        <td>Email</td>
						        <td>{{$extra->email}}</td>
						      </tr>
						      <tr>
						        <td>Reference ID</td>
						        <td>{{$pgp->reference}}</td>
						      </tr>
						      <tr>
						        <td>Transaction Date</td>
						        <td>{{$pgp->created_at}}</td>
						      </tr>
						      <tr>
						        <td>Amount</td>
						        <td>R {{$extra->amountZAR}}(ZAR) => [ {{$extra->amountUSD}}(USD) ]</td>
						      </tr>
					
						    </tbody>
						  </table>

					</div>

					<div style="text-align: center;">

					@if($extra->device_type == 'ANDROID')

					<button id="close_btn"  class="btn btn-success" type="button" value="@if($CHECKSUM != $generatedChecksum || $TRANSACTION_STATUS != 1){{'error'}}@else{{'success'}}@endif" onclick="ok.performClick(this.value);">Done!!</button>

					@elseif($extra->device_type == 'IOS') 


					<a id="close_btn" class="btn btn-success" type="button" 
						@if($CHECKSUM != $generatedChecksum || $TRANSACTION_STATUS != 1)
							href="inapp://error"
						@else
							href="inapp://success"
						@endif
						
					>Done!!</a>

					@endif

					</div>

			</div>			
			<div class="col-md-3"></div>			
		</div>
    </body>
</html>
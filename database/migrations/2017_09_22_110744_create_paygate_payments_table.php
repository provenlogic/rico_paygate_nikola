<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaygatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paygate_payments', function (Blueprint $table) {
            $table->increments('id');
            
            $table->bigInteger('request_id');
            $table->string('reference_id', 256);
            $table->text('data');
            $table->enum('status', ['SUCCESS', 'FAILED', 'PENDING'])->default('SUCCESS');
 
            $table->timestamps();
            $table->softDeletes();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('paygate_payments');
    }
}
